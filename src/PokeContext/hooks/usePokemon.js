import {useEffect, useState} from 'react'
import axios from 'axios'

export default function usePokemon(){
    const [pokedex, setPokedex] = useState(null)
    const [pokemon, setPokemon] = useState(null)
    
    function getPokeData(){
        axios.get("https://pokeapi.co/api/v2/pokemon")
        .then((data)=>{
            var lista = data.data.results             
            console.log(lista)
            setPokedex(lista)
            })
        }
    useEffect(()=>{
        getPokeData()    
    },[])

    return {pokedex, pokemon, setPokedex, setPokemon}
}