import PokeContext from "./Context";
import usePokemon from './hooks/usePokemon';

const PokeProvider = ({children}) =>{
    const {pokedex, pokemons} = usePokemon()
    return (
        <PokeContext.Provider value={{pokedex, pokemons}}>
            {children}
        </PokeContext.Provider>
    )
}

export default PokeProvider