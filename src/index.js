import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

import PokeProvider from './PokeContext/Provider';


ReactDOM.render(
  <PokeProvider>
    <App />
  </PokeProvider>,
  document.getElementById('root')
);

reportWebVitals();
