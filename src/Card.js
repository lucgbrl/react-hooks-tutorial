export default function Card(props){    
    return(
        <div className="card">
            <div className="card-body text-dark">
                <img src={props.src} alt="props.name" />
                <p>{props.name.toUpperCase()}</p>
                <p>{props.url}</p>
                <button className="btn btn-warning">Detalhes</button>
            </div>
        </div>
    )
}

