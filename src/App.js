import axios from 'axios'
import React, {useContext, useState} from 'react' 
import PokeContext from './PokeContext/Context'

import Card from './Card'
import Nav from './Components/Nav'

function App() {

  var {pokedex} = useContext(PokeContext)
  var [captura, setCaptura] = useState(null)
  var [detalhes, setDetalhes] = useState(null)
  var [consulta, setConsulta] = useState(null)

  function verDetalhes(url){
    axios.get(url).then((data)=>{
      setCaptura(data.data.species)
      setDetalhes(data.data)
      sessionStorage.setItem("@App:pokedex", JSON.stringify(data.data.species))
    })
  }

  function fazConsulta(pokemon){
    console.log(pokemon)
    axios.get(`https://pokeapi.co/api/v2/pokemon/${pokemon}`)
      .then((data)=>{
        setConsulta(data.data)
    })
  }

  return (    
    <> 
    <Nav />
    <div className="container mt-5">           
      <div className="row">     
      <div className="col-md-12 mt-5 mb-5 text-white">
          <h3>Pokédex</h3>     
          <h3>Nome ou número</h3>
        <form>
          <div className="row">
            <div className="col-md-4">
              <input className="form-control mb-3" placeholder="Digite o nome do Pokemon" onChange={(e)=>{fazConsulta(e.target.value)}}/>
            </div>
            <div className="col-md-6">
              <button className="btn btn-warning">Buscar Pokemon</button>
            </div>
          </div>
        </form>
          {consulta ? JSON.stringify(consulta) : "Nenhuma consulta realizada"}
        <br />
        <br />
        {captura 
          ? <Card name={captura.name} url={captura.url}/>
          : "Nenhum Pokemon selecionado!" }
      </div>
      <div className="col-md-12 mb-3 d-flex justify-content-between">
        <button className="btn btn-primary"> <i className="fa fa-sync"></i> Surpreenda-me!</button>
        <button className="btn btn-warning"> <i className="fa fa-plus"></i> Carregar mais pokemons</button>
      </div>
      {pokedex
        ? <>          
          {pokedex.map((pokemon, index)=>{
            return (
              <div className="col-md-3 pokemon" key={pokemon.name}>                
                <div className="card card-body mb-3">
                  <center>
                    <img alt={pokemon.name} src={`http://pokeres.bastionbot.org/images/pokemon/${index+1}.png`} width="100"/>
                    <p>{pokemon.name.toUpperCase()} </p>
                    <button className="btn btn-danger btn-block" onClick={(e)=>{verDetalhes(pokemon.url)}}>Detalhes</button>
                  </center>
                </div>
              </div>
            )
          })}
        </>
        : "Nenhum pokemon encontrado!"
      }
      </div>
    </div>
    </>
  );
}

export default App;
